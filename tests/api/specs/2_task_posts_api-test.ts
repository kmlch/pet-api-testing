import { expect } from "chai";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { UsersController } from "../lib/controllers/users.controller";

const register = new RegisterController();
const auth = new AuthController();
const posts = new PostsController();
const comments = new CommentsController();
const users = new UsersController();

const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Task 2: Posts Tests", () => {
    let accessToken: string;
    let userId: number;
    let postId: number;
    let userEmail = "testNewUser002@gmail.com";
    let userPassword = "passw0rd";

    before(`Register new user and get the token`, async () => {
        let userData: object = {
            avatar: "avatarTest",
            email: userEmail,
            userName: "testNewUser002",
            password: userPassword,
        };

        let newUserResponse = await register.registerUser(userData);
        checkStatusCode(newUserResponse, 201);

        let loginResponse = await auth.login(userEmail, userPassword);
        checkStatusCode(loginResponse, 200);

        userId = newUserResponse.body.user.id;
        accessToken = loginResponse.body.token.accessToken.token;
    });

    after(`Delete user`, async () => {
        await users.deleteUser(userId, accessToken);
    });

    it(`add a new post`, async () => {
        let postData: object = {
            authorId: userId,
            previewImage: "test",
            body: "test",
        };

        let response = await posts.createPost(postData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response, schemas.schema_post);

        expect(response.body.author.id, `Author ID should be ${userId}`).to.be.jsonSchema(userId);

        postId = response.body.id;
    });

    it(`get the posts collection`, async () => {
        let response = await posts.getAllExistingPosts();
        let post = response.body.find((item) => item.id === postId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
        checkJsonSchema(response, schemas.schema_allPosts);

        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
        expect(post.author.id, `Author ID should be ${userId}`).to.be.equal(userId);
        expect(post.body, "Post body should be correct").to.be.equal("test");
    });

    it(`add reation to the post`, async () => {
        let postData: object = {
            entityId: postId,
            isLike: true,
            userId: userId,
        };

        let response = await posts.addLikeReactionToPost(postData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`get the posts collection and verify added reaction`, async () => {
        let response = await posts.getAllExistingPosts();
        let post = response.body.find((item) => item.id === postId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
        checkJsonSchema(response, schemas.schema_allPosts);

        expect(post.reactions[0].isLike, `IsLike field should be true`).to.be.equal(true);
        expect(post.reactions[0].user.id, `Reaction author should be current user`).to.be.equal(userId);
    });

    it(`add comment to the post`, async () => {
        let postData: object = {
            authorId: userId,
            postId: postId,
            body: "some new comment testing",
        };

        let response = await comments.addCommentToPost(postData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response, schemas.schema_postComment);

        expect(response.body.author.id, "Author ID should be correct").to.be.equal(userId);
        expect(response.body.body, "Comment body should be correct").to.be.equal("some new comment testing");
    });

    it(`get the posts collection and verify added comment`, async () => {
        let response = await posts.getAllExistingPosts();
        let createdPost = response.body.find((item) => item.id === postId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
        checkJsonSchema(response, schemas.schema_allPosts);

        expect(createdPost.comments[0].body, `Comment body should be correct`).to.be.equal(
            "some new comment testing"
        );
        expect(createdPost.comments[0].author.id, `Comment author should be current user`).to.be.equal(userId);
    });

    it(`add comment to the post with invalid type of postID`, async () => {
        let invalidTypePostId = "test";
        let postData: object = {
            authorId: userId,
            postId: invalidTypePostId,
            body: "some new comment testing",
        };

        let response = await comments.addCommentToPost(postData, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });

    it(`add a new post with invalid type authorID`, async () => {
        let invalidTypeAuthorId = "test";
        let postData: object = {
            authorId: invalidTypeAuthorId,
            previewImage: "test",
            body: "test",
        };

        let response = await posts.createPost(postData, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });

    it(`add a new post with non-existing authorID`, async () => {
        let invalidAuthorId = 46897979887;

        let postData: object = {
            authorId: invalidAuthorId,
            previewImage: "test",
            body: "test",
        };

        let response = await posts.createPost(postData, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });
});
