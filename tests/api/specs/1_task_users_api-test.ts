import { expect } from "chai";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const register = new RegisterController();
const users = new UsersController();
const auth = new AuthController();

const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Task 1: Users Tests", () => {
    let accessToken: string;
    let userId: number;
    let userEmail = "testNewUser009@gmail.com";
    let userPassword = "passw0rd";
    let userName = "testNewUser009";

    before(`Register new user, login and get the token`, async () => {
        let userData: object = {
            avatar: "avatarTest",
            email: userEmail,
            userName: userName,
            password: userPassword,
        };

        let newUserResponse = await register.registerUser(userData);
        checkStatusCode(newUserResponse, 201);
        checkResponseTime(newUserResponse, 1000);
        checkJsonSchema(newUserResponse, schemas.schema_registerUser);

        let loginResponse = await auth.login(userEmail, userPassword);
        checkStatusCode(loginResponse, 200);
        checkResponseTime(loginResponse, 1000);
        checkJsonSchema(loginResponse, schemas.schema_authorizedUser);

        expect(loginResponse.body.user.userName, `User Name should be ${userName}`).equals(userName);
        expect(loginResponse.body.user.email, "User Email should be correct").equals(userEmail);

        userId = newUserResponse.body.user.id;
        accessToken = loginResponse.body.token.accessToken.token;
    });

    it(`get the users collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
        checkJsonSchema(response, schemas.schema_allUsers);
    });

    it(`get current user by token`, async () => {
        let response = await users.getCurrentUserByToken(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response, schemas.schema_user);

        expect(response.body.userName, `User Name should be ${userName}`).equals(userName);
        expect(response.body.id, "User ID should be correct").equals(userId);
        expect(response.body.email, "User Email should be correct").equals(userEmail);
        expect(response.body.avatar, "User Avatar should be correct").equals("avatarTest");
    });

    it(`update user data`, async () => {
        let userData: object = {
            id: userId,
            avatar: "newAvatarUpdates",
            email: userEmail,
            userName: userName,
        };

        let response = await users.updateUser(userData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`get current user by token and verify updated field`, async () => {
        let response = await users.getCurrentUserByToken(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response, schemas.schema_user);

        expect(response.body.avatar, "Avatar value should be up to date").equals("newAvatarUpdates");
    });

    it(`get user by ID`, async () => {
        let response = await users.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response, schemas.schema_user);

        expect(response.body.userName, `User Name should be ${userName}`).equals(userName);
    });

    it(`get user by non-existing ID`, async () => {
        let invalidUserId = 1999239;
        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 404);
        checkResponseTime(response, 1000);
    });

    it(`get user by invalid id type`, async () => {
        let invalidTypeUserId = "test";
        let response = await users.getUserById(invalidTypeUserId);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });

    it(`delete user`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`delete non-exitisng user`, async () => {
        let invalidUserId = 1999293;
        let response = await users.deleteUser(invalidUserId, accessToken);

        checkStatusCode(response, 404);
        checkResponseTime(response, 1000);
    });
});
