import { expect } from "chai";
import { checkStatusCode, checkResponseTime, checkJsonSchema } from "../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";

const register = new RegisterController();

const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Task 3: Register User Tests With Invalid Email - test data set usage", () => {
    let invalidEmailDataSet = [
        "@domain.com",
        "user@",
        "userdomain.com",
        "userdomain" /*,"user@.com", "user@domain"*/,
    ];

    invalidEmailDataSet.forEach((email) => {
        it(`register user with invalid email: "${email}"`, async () => {
            let userData: object = {
                avatar: "avatar",
                email: email,
                userName: "someUserName",
                password: "passw00rd",
            };

            let response = await register.registerUser(userData);
            checkStatusCode(response, 400);
            checkResponseTime(response, 1000);
        });
    });
});
