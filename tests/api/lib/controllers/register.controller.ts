import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {
    async registerUser(userData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(userData)
            .send();
        return response;
    }
}
