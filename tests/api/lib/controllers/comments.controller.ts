import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class CommentsController {
    async addCommentToPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}